# SSHD
SSH service.

## Usage Example

* As jump server

## Build
```
docker build -t sshd .
```

## How To Use

* [Optional] Prepare host key file
```
mkdir -p /tmp/keys
# put host keys to /tmp/keys
```

* Prepare authorized_keys
```
echo 'ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAIEAmK4ItOzqSZQkl3YFwe+FuiHwt+1EsVtfstjgvNysnaZxS2RQtbyNkE1Zy7sW3rlIyxy/RXRvOxSNn8TmcZ2oLAhjj33jS9xTBCfadHDB1Ud5TzU5qG/i2liGnzUclSNARafkyZgtDTQtx9xED4VjJArrHfHWbJ6RwmnA9LogLB8=' > /tmp/keys/authorized_keys
```

* Start container
```
docker run --name sshd -d -v /tmp/keys:/data -p 22:22 sshd
```

* Connect
```
ssh miffyliye@localhost
```

#!/bin/bash

set -ouo pipefail

adduser --disabled-password --gecos "" $USERNAME  
mkdir -p /home/$USERNAME/.ssh 
chown $USERNAME:$USERNAME /home/$USERNAME/.ssh

cp /data/authorized_keys /home/$USERNAME/.ssh/
chown $USERNAME:$USERNAME /home/$USERNAME/.ssh/authorized_keys

cp -r /data/*_key* /etc/ssh/ 
cd /etc/ssh && chmod 600 *_key && chmod 644 *_key.pub 

if [ ${SUDO} -eq "1" ]; then
  usermod -aG sudo $USERNAME
  echo "$USERNAME         ALL = (ALL) NOPASSWD: ALL" >> /etc/sudoers
fi

if [ $# -eq 0 ]; then
  exec /usr/sbin/sshd -D
else
  exec "$@"
fi